package com.example.rest;

import com.example.model.CourseModel;
import com.example.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Get-Up on 4/30/2017.
 */
@RestController
@RequestMapping("/rest")
public class CourseRestController {
    @Autowired
    CourseService courseService;

    @RequestMapping("/course/view/{idCourse}")
    public CourseModel view(@PathVariable(value = "idCourse") String idCourse){
        CourseModel courseModel = courseService.selectCourse(idCourse);
        return courseModel;
    }

    @RequestMapping("/course/viewall")
    public List<CourseModel> viewAll(){
        List<CourseModel> listCourse = courseService.selectAllCourse();
        return listCourse;
    }
}
