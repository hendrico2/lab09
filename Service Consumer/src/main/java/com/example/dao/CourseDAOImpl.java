package com.example.dao;

import com.example.model.CourseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Get-Up on 4/30/2017.
 */
@Service
public class CourseDAOImpl implements CourseDAO{
    @Autowired
    RestTemplate restTemplate;

    @Override
    public CourseModel selectCourse(String idCourse) {
        CourseModel courseModel = restTemplate.getForObject("http://localhost:8080/rest/course/view/"+idCourse,CourseModel.class);
        return courseModel;
    }
}
