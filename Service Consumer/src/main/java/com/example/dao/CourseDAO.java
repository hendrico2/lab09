package com.example.dao;

import com.example.model.CourseModel;

/**
 * Created by Get-Up on 4/30/2017.
 */
public interface CourseDAO {
    CourseModel selectCourse(String idCourse);
}
