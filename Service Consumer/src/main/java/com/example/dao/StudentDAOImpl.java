package com.example.dao;

import com.example.model.StudentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Get-Up on 4/30/2017.
 */
@Service
public class StudentDAOImpl implements StudentDAO {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public StudentModel selectStudent(String npm) {
        StudentModel student = restTemplate.getForObject(
                "http://localhost:8080/rest/student/view/" + npm, StudentModel.class);
        return student;
    }

    @Override
    public List<StudentModel> selectAllStudent() {
        /*List<StudentModel> listStudent= restTemplate.getForEntity(
                "http://localhost:8080/rest/student/viewall",List<StudentModel>);
        */
        ResponseEntity<StudentModel[]> responseEntity = restTemplate.getForEntity(
                "http://localhost:8080/rest/student/viewall", StudentModel[].class);

        List<StudentModel> listStudent = Arrays.asList(responseEntity.getBody());
        return listStudent;
    }
}
