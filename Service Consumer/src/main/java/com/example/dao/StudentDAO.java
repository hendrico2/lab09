package com.example.dao;

import com.example.model.StudentModel;

import java.util.List;

/**
 * Created by Get-Up on 4/30/2017.
 */
public interface StudentDAO {
    StudentModel selectStudent(String npm);

    List<StudentModel> selectAllStudent();
}
