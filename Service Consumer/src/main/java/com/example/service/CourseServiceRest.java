package com.example.service;

import com.example.dao.CourseDAO;
import com.example.model.CourseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * Created by Get-Up on 4/30/2017.
 */
@Slf4j
@Service
@Primary
public class CourseServiceRest implements CourseService{
    @Autowired
    CourseDAO courseDAO;

    @Override
    public CourseModel selectCourse(String idCourse) {
        log.info ("REST - select course with id course {}", idCourse);
        return courseDAO.selectCourse(idCourse);
    }
}
